# SPL tool (VisionFive2)

**NOTE** Development has moved to [weathered-steel/spl_tool](https://codeberg.org/weathered-steel/spl_tool).

Port of the C implementation of StarFive's [`spl_tool`](https://github.com/starfive-tech/Tools/tree/master/spl_tool), which is originally written by [`strlcat`](https://github.com/strlcat).

**WARNING** This tool is in the very earliest stages of development. It still requires a test suite, fuzzing harnesses for file formats, and real-world testing.

## Usage

To use the CLI applicatiom, compile with the `cli` feature:

```
$ cd spl_tool
$ cargo run --features cli -- --file <path-to-spl-image> --create-spl-header
# To see a full list of options
$ cargo run --features cli -- --help
```

### Library example

```rust
use spl_tool::{crc32, crc32_final, Result, HeaderConf, UbootSplHeader};
use spl_tool::{DEF_CRC32_IV, DEF_CRC32_SV, SPL_HEADER_LEN};

// Dummy example of the U-Boot SPL image size
// Real value should be read from SPL image file
const UBOOT_IMG_SIZE: usize = 0xf00d;

// Dummy data for the SPL image data
let spl = [0xff; UBOOT_IMG_SIZE];

// Calculate the main CRC-32 rounds
let v = crc32(DEF_CRC32_IV, DEF_CRC32_SV, spl.as_ref());
// Calculate the final CRC-32 round
let crc = crc32_final(v);

let conf = HeaderConf::new().with_name("u-boot-spl.bin");

let hdr = UbootSplHeader::new()
    .with_bofs(conf.bofs())
    .with_vers(conf.vers())
    .with_fsiz(UBOOT_IMG_SIZE as u32)
    // Set calculated CRC-32 in the header
    .with_crcs(crc);

// Convert header to bytes
let _hdr_bytes: [u8; SPL_HEADER_LEN] = hdr.into();

// ... do cool stuff with your SPL header
// ... usually, prepend it to the SPL image data in a new SPL image file
```

## Installation

The CLI application requires the `cli` feature:

```
$ cd spl_tool
$ cargo install --features cli --path .
```

## no-std compatibility

The library portion of `spl_tool` is `no-std` compatible by default, and can be used in embedded/bare-metal contexts.

## Alternatives

- `spl_tool` (C): <https://github.com/starfive-tech/Tools/tree/master/spl_tool>
- `vf2-header` (Rust): <https://github.com/jonirrings/vf2-header>

## License

`spl_tool` Rust is licensed under the same GPLv2+ license as the original C implementation.
