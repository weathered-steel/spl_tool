// SPDX-License-Identifier: GPL-2.0+

//! SPL header tool for the StarFive VisionFive2 board.
//!
//! Based on the C implementation: <https://github.com/starfive-tech/Tools/tree/master/spl_tool>
//!
//! ```rust
//! use spl_tool::{crc32, crc32_final, Result, HeaderConf, UbootSplHeader};
//! use spl_tool::{DEF_CRC32_IV, DEF_CRC32_SV, SPL_HEADER_LEN};
//!
//! // Dummy example of the U-Boot SPL image size
//! // Real value should be read from SPL image file
//! const UBOOT_IMG_SIZE: usize = 0xf00d;
//!
//! // Dummy data for the SPL image data
//! let spl = [0xff; UBOOT_IMG_SIZE];
//!
//! // Calculate the main CRC-32 rounds
//! let v = crc32(DEF_CRC32_IV, DEF_CRC32_SV, spl.as_ref());
//! // Calculate the final CRC-32 round
//! let crc = crc32_final(v);
//!
//! let conf = HeaderConf::new().with_name("u-boot-spl.bin");
//!
//! let hdr = UbootSplHeader::new()
//!     .with_bofs(conf.bofs())
//!     .with_vers(conf.vers())
//!     .with_fsiz(UBOOT_IMG_SIZE as u32)
//!     // Set calculated CRC-32 in the header
//!     .with_crcs(crc);
//!
//! // Convert header to bytes
//! let _hdr_bytes: [u8; SPL_HEADER_LEN] = hdr.into();
//!
//! // ... do cool stuff with your SPL header
//! // ... usually, prepend it to the SPL image data in a new SPL image file
//! ```

#![no_std]

mod crc32;
mod error;
mod spl_header;

pub use crc32::*;
pub use error::*;
pub use spl_header::*;
